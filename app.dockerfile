FROM python:3.6-alpine

# RUN apk update && \
    apk upgrade && \
    # apk add wget unzip
    # apk add git

ADD . /code
RUN mkdir /code
WORKDIR /code

# RUN echo "Starting ${STARTIME}" && git clone -b master https://valdergallo@bitbucket.org/valdergallo/bluegreen.git .

# RUN wget --no-check-certificate -O master.zip "https://bitbucket.org/valdergallo/bluegreen/get/master.zip?${TIMESTAMP}"
# RUN unzip -j master.zip
# RUN rm master.zip

##############################################################################
# Clean up
##############################################################################
# RUN rm -rf /var/cache/apk/*; \
#    # apk del wget unzip; \
#    apk del git; \
#    echo "End of /RUN";

CMD ["python", "app.py"]
